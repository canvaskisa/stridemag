module.exports = {
	feature: {
		date: '12.04.2017',
		title: '«Это как концерт Guns’N’Roses», — Наташа Давтян о том, что такое волонтер на забеге',
		category: 'Правила бега',
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1494949999/feature_dwypap.jpg'
	},
	featureCards: [{
		date: '12.04.2017',
		title: '«Это как концерт Guns’N’Roses», — Наташа Давтян о том, что такое волонтер на забеге',
		category: 'Правила бега',
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1494949998/img0_ol3fnk.jpg'
	}, {
		date: '12.04.2017',
		title: '«Это как концерт Guns’N’Roses», — Наташа Давтян о том, что такое волонтер на забеге',
		category: 'Правила бега',
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1494949998/img1_iunapj.jpg'
	}],
	slides: [{
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1493928181/shoe_q5mghk.png',
		title: 'Мне говорили: дурак, мало того, что не видит, еще и лоб расшибет. Хотелось бросить. Но потихоньку, с 50 м, дошел до полутора километров.',
		text: 'Читайте о новых Nike от Артемия Лебедева'
	}, {
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1493928181/shoe_q5mghk.png',
		title: 'Мне говорили: дурак, мало того, что не видит, еще и лоб расшибет. Хотелось бросить. Но потихоньку, с 50 м, дошел до полутора километров.',
		text: 'Читайте о новых Nike от Артемия Лебедева'
	}, {
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1493928181/shoe_q5mghk.png',
		title: 'Мне говорили: дурак, мало того, что не видит, еще и лоб расшибет. Хотелось бросить. Но потихоньку, с 50 м, дошел до полутора километров.',
		text: 'Читайте о новых Nike от Артемия Лебедева'
	}, {
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1493928181/shoe_q5mghk.png',
		title: 'Мне говорили: дурак, мало того, что не видит, еще и лоб расшибет. Хотелось бросить. Но потихоньку, с 50 м, дошел до полутора километров.',
		text: 'Читайте о новых Nike от Артемия Лебедева'
	}],
	cards: [{
		date: '12.04.2017',
		title: '«Это как концерт Guns’N’Roses», — Наташа Давтян о том, что такое волонтер на забеге',
		category: 'Правила бега',
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1494949998/img2_n2lboo.jpg'
	}, {
		date: '12.04.2017',
		title: '«Это как концерт Guns’N’Roses», — Наташа Давтян о том, что такое волонтер на забеге',
		category: 'Правила бега',
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1494949998/img4_ajmbgv.jpg'
	}, {
		date: '12.04.2017',
		title: '«Это как концерт Guns’N’Roses», — Наташа Давтян о том, что такое волонтер на забеге',
		category: 'Правила бега',
		image: 'http://res.cloudinary.com/canvaskisa/image/upload/v1494949998/img5_nrdst7.jpg'
	}]
};
